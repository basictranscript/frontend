import * as React from 'react';
import { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { DataGrid } from '@mui/x-data-grid';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import LinearProgress from '@mui/material/LinearProgress';

<link
  rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
/>

const H1 = styled('h1')(({ theme }) => ({
  ...theme.typography.button,
  backgroundColor: theme.palette.background.paper,
  padding: theme.spacing(0),
}));
  
function BasicInfo(props) {
  return (
    <Card sx={{ minWidth: 275 }} variant = "outlined">
      <CardContent>
        <Typography sx={{ fontSize: 24 }} color="text.secondary" gutterBottom>
          Academic Transcript
        </Typography>
        <Typography sx={{ mb: 1, fontSize: 18  }} color="text.secondary">
          Name of Student : {props.data.name}
        </Typography>
        <Typography sx={{ mb: 1, fontSize: 18 }} color="text.secondary">
          Year : {props.data.year}
        </Typography>
        <Typography sx={{ mb: 1, fontSize: 18 }} color="text.secondary">
          Level : {props.data.level}
        </Typography>
        <Typography sx={{ mb: 1, fontSize: 18 }} color="text.secondary">
          Faculty : {props.data.faculty}
        </Typography>
        <Typography sx={{ mb: 1, fontSize: 18 }} color="text.secondary">
          Roll Number : {props.data.roll}
        </Typography>
      </CardContent>
    </Card>
  );
}

function ProgressBar() {
  return (
    <Box sx={{ width: '100%' }}>
      <H1 style={{'paddingTop':10, fontSize:18}}>Loading</H1>
      <LinearProgress />
    </Box>
  );
}

function Semester(props) {
    const columns = [
        { field: 'code', headerName: 'Code', width: 130 },
        { field: 'name', headerName: 'Subject Name', width: 350 },
        { field: 'passedyear', headerName: 'Exam Year', width: 110 },
        { field: 'fm', headerName: 'Full Marks', type: 'number', width: 110 },
        { field: 'pm', headerName: 'Pass Marks', type: 'number', width: 120 },
        { field: 'om', headerName: 'Obtained Marks', type: 'number', width: 150 },
        { field: 'percent', headerName: 'Percentage (%)', type: 'number', width: 150, valueGetter: (params) =>
        `${Math.round((params.row.om/params.row.fm * 100 + Number.EPSILON)*100)/100}`}
      ];
    let rows = JSON.parse(JSON.stringify(props.data.subjects));
    return (
      <div style={{ padding:30, height: 400, width: '100%' }}>
      <H1 style={{ fontSize: 18, 'textAlign': 'center', mx:30 }}>Year {Math.ceil(props.data.number/2)} Part {(1 + props.data.number)%2 + 1}</H1>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={15}
          rowsPerPageOptions={[15]}
          density = {'standard'}
          sx={{ fontSize: 16, mx: 20, 'textAlign': 'center'}}
        />
      </div>
    );
  }

function reduceMarks(previousValue,currentValue){
  currentValue.fm += previousValue.fm;
  currentValue.pm += previousValue.pm;
  currentValue.om += previousValue.om;
  currentValue.id = previousValue.id;
  currentValue.name = previousValue.name;
  return currentValue;
}

function Summary(props){
  const columns = [
    { field: 'id', headerName: 'Semester', type: 'number', width: "150" },
    { field: 'fm', headerName: 'Full Marks', type: 'number', width: 130 },
    { field: 'pm', headerName: 'Pass Marks', type: 'number', width: 130 },
    { field: 'om', headerName: 'Obtained Marks', type: 'number', width: 150 },
    { field: 'percent', headerName: 'Percentage (%)', width: 150, type: 'number', valueGetter: (params) =>
    `${Math.round((params.row.om/params.row.fm * 100 + Number.EPSILON)*100)/100}`,
    }
  ];

  let semesters = JSON.parse(JSON.stringify(props.studentData.semesters));
  
  let summary = semesters.map(
    (semester)=>{
        return semester.subjects.reduce(reduceMarks,{fm:0, pm:0, om:0, percent:0, id:semester.number});
    }
  );
  let new_summary = JSON.parse(JSON.stringify(summary));
  let Total = new_summary.reduce(reduceMarks,{name:"Total", fm:0, pm:0, om:0, percent:0, id:'Total' });
  summary.push(Total);
  return (
    <div style={{ padding:30, height: 400, width: '100%' }}>
    <H1 style={{ fontSize: 18, 'textAlign': 'center', mx:30 }}>Summary of all Semesters</H1>
      <DataGrid
        rows={summary}
        columns={columns}
        pageSize={9}
        rowsPerPageOptions={[9]}
        density = {'standard'}
        sx={{ fontSize: 16, mx: 20}}
      />
    </div>);
}

function Transcript () {
    const [studentData, setStudentData] = useState(null)
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
      const api_url = "http://localhost:5000/data";
      fetch(api_url, {'Access-Control-Allow-Origin': '*'}).then(
        response => {return response.json()}
      ).then(
       studentData => {
          setStudentData(studentData);
          setIsLoading(false);
        }
      );
    },[]);
    function render_all_semesters(){
      document.title = "Transcript - " + studentData.name
      return (
        <div>
          <Card sx={{ minWidth: 100 }} variant="outlined" >
              <CardContent>
                  <Summary studentData={studentData}></Summary>
                  {studentData.semesters.map(
                      (_semester,index)=>{
                          return (<Semester key={index} data={_semester}/>);
                      }
                  )}
              </CardContent>
          </Card>
        </div>
      );
    }
    return <div>{isLoading?<ProgressBar></ProgressBar>:<div>
      <BasicInfo data={studentData}></BasicInfo>
          <div>
              {render_all_semesters(studentData)}
          </div>
      </div>}</div>;
  }
    
ReactDOM.render(<Transcript />, document.querySelector('#root'));